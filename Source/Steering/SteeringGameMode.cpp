// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "Steering.h"
#include "SteeringGameMode.h"
#include "GameFramework/DefaultPawn.h"

ASteeringGameMode::ASteeringGameMode()
{
	// set default pawn class to our flying pawn
	DefaultPawnClass = ADefaultPawn::StaticClass();

}
