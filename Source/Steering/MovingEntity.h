/**
  @Author: Hendranus Vermeulen 
  @Date: 2020-02-25 08:27:18 
  @Last Modified by:   Hendranus Vermeulen 
  @Last Modified time: 2020-02-25 08:27:18 
 
	Base class from which all steered agents derive, it exposes functions and properties for Blueprint configuration
 
 */

#pragma once

#include "GameFramework/Actor.h"
#include "MovingEntity.generated.h"

UCLASS()
class STEERING_API AMovingEntity : public AActor
{
	GENERATED_BODY()
	
public:	

	enum { default_entity_type = -1 };

	// Sets default values for this actor's properties
	AMovingEntity();

	// StaticMesh component that will be the visuals for our flying pawn
	UPROPERTY(Category = Mesh, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* boidMesh;

	// Return the mesh
	FORCEINLINE class UStaticMeshComponent* GetMesh() const { return boidMesh; }

	UFUNCTION(BlueprintCallable, Category = "MovingEntity")
	FVector   Velocity()const{ return _Velocity; }
	void      SetVelocity(const FVector& NewVel){ _Velocity = NewVel; }

	FVector   Side()const{ return _Side; }

	bool      IsSpeedMaxedOut()const{ return MaxSpeed*MaxSpeed >= _Velocity.Size(); }
	float     Speed()const{ return _Velocity.Size(); }
	float     SpeedSq()const{ return _Velocity.SizeSquared(); }
	float     GetMaxSpeed()const{ return MaxSpeed; }
	float     GetMaxForce()const{ return MaxForce; }
	FVector   Heading()const{ return _Heading; }
	void      SetHeading(FVector new_heading);

	UFUNCTION(BlueprintCallable, Category = "MovingEntity")
	bool      RotateHeadingToFacePosition(FVector target, float DeltaSeconds);

	UFUNCTION(BlueprintCallable, Category = "Steering")
		TArray<AMovingEntity*> GetNeighbors() { return neighbors; }

	float	GetDecelFractDist()const{ return DecelFractDist; }
	float	GetWanderRadius()const{ return WanderRadius; }
	float	GetWanderJitter()const{ return WanderJitter; }
	float	GetWanderDistance()const{ return WanderDistance; }

	FVector   Pos()const{ return _Pos; }
	void      SetPos(FVector new_pos){ _Pos = new_pos; }

	int       ID()const{ return _ID; }

	bool      IsTagged()const{ return _Tag; }
	void      Tag(){ _Tag = true; }
	void      UnTag(){ _Tag = false; }

	int       EntityType()const{ return _EntityType; }
	void      SetEntityType(int new_type){ _EntityType = new_type; }

	// Give each entity a unique ID
	int NextValidID(){ static int NextID = 0; return NextID++; }

	UWorld*const         World()const{ return _World; }

private:

	// Unique ID
	int         _ID;
	int         _EntityType;
	bool        _Tag;
	UWorld*            _World;

protected:

	// Boid location in the environment
	FVector _Pos;

	FVector _Scale;

	FVector	_Velocity;

	TArray<AMovingEntity*> neighbors;

	// Normalized heading of boid
	FVector	_Heading;

	// Perpendicular to the heading
	FVector    _Side;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MovingEntity")
	float       Mass = 100.0f;

	// Maximum speed this boid may travel
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MovingEntity")
	float       MaxSpeed = 1000.0f;

	// Maximum force this boid can use
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MovingEntity")
	float      MaxForce = 1000.0f;

	// Maximum rate (radians per second) boid can rotate    
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MovingEntity")
	float       MaxTurnRate = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Wandering")
	float        WanderJitter = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Wandering")
	float        WanderRadius = 200.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Wandering")
	float        WanderDistance = 50;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MovingEntity")
	float DecelFractDist = 0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Steering")
	float        WeightSeparation = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Steering")
	float        WeightCohesion = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Steering")
	float        WeightAlignment = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Steering")
	float        WeightWander = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Steering")
	float        WeightObstacleAvoidance = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Steering")
	float        WeightSeek = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Steering")
	float        WeightFlee = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Steering")
	float        WeightArrive = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Steering")
	float        WeightPursuit = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Steering")
	float        WeightOffsetPursuit = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Steering")
	float        WeightHide = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Steering")
	float        WeightEvade = 0.5f;


	
};
