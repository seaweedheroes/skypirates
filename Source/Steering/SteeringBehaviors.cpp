/**
  @Author: Hendranus Vermeulen 
  @Date: 2020-02-25 08:27:50 
  @Last Modified by:   Hendranus Vermeulen 
  @Last Modified time: 2020-02-25 08:27:50 
 
 	SteeringBehaviors encapsulates different steering behaviours and calcultes the combined result of the forces
 
 */

#include "Steering.h"
#include "MovingEntity.h"
#include "DrawDebugHelpers.h"
#include "SteeringBehaviors.h"

SteeringBehaviors::SteeringBehaviors(AMovingEntity* boid)
{
	_Boid = boid;
}

SteeringBehaviors::~SteeringBehaviors()
{
}

/**
	Calculates the combined result of the forces

	@float DeltaSeconds
	@return FVector Steering Force
*/
FVector SteeringBehaviors::Calculate(float DeltaSeconds)
{
	_SteeringForce = FVector::FVector(0.0f, 0.0f, 0.0f);
	_SteeringForce = CalculatePrioritized(DeltaSeconds);
	return _SteeringForce;
}

/**
	Transforms local position reltive to moving entity to world position

	@FVector localPoint 
	@AMovingEntity* actor
	@return FVector World position vector
*/
FVector SteeringBehaviors::LocalToWorld(FVector localPoint, AMovingEntity* actor)
{
	FRotator currentRotation = actor->GetMesh()->GetComponentRotation();
	FRotationTranslationMatrix uber = FRotationTranslationMatrix(currentRotation, actor->Pos());
	FVector target = uber.TransformPosition(localPoint);
	return target;
}

/**
	Transforms world position reltive to moving entity to local position

	@FVector worldPoint
	@AMovingEntity* actor
	@return FVector Local position vector
*/
FVector SteeringBehaviors::WorldToLocal(FVector worldPoint, AMovingEntity* actor)
{
	FTransform trans = actor->GetMesh()->GetComponentTransform();
	return trans.InverseTransformPosition(worldPoint);
}

/**
	Truncate vector to min magnitude

	@FVector &actualVector
	@float min
	@return FVector actualVector
*/
FVector SteeringBehaviors::TruncateVector(FVector &actualVector, float min)
{
	if (actualVector.Size() > min)
	{
		actualVector.Normalize();
		actualVector *= min;
	}
	return actualVector;
}

/**
	Sums up all the active behaviors using assigned weights updating steering force

	@float DeltaSeconds
	@return FVector SteeringForce
*/
FVector SteeringBehaviors::CalculateWeightedSum(float DeltaSeconds)
{
	FVector force;

	if (On(separation))
	{
		force += Separation(_Boid->GetNeighbors()) * _WeightSeparation;
	}

	if (On(allignment))
	{
		force += Alignment(_Boid->GetNeighbors()) * _WeightAlignment;
	}

	if (On(cohesion))
	{
		force += Cohesion(_Boid->GetNeighbors()) * _WeightCohesion;
	}

	if (On(wander))
	{
		force += Wander(DeltaSeconds) * _WeightWander;
	}

	if (On(pursuit))
	{

		force += Pursuit(_TargetAgent1) * _WeightPursuit;
	}

	TruncateVector(force, _Boid->GetMaxForce());

	return force;
}

/**
	Calculate accumulated steering force in order of behaviours priority using weights

	@float DeltaSeconds
	@return FVector SteeringForce
*/
FVector SteeringBehaviors::CalculatePrioritized(float DeltaSeconds)
{
	FVector force;

	if (On(separation))
	{
		force = Separation(_Boid->GetNeighbors()) * _WeightSeparation;
		if (!AccumulateForce(_SteeringForce, force)) return _SteeringForce;
	}

	if (On(allignment))
	{
		force = Alignment(_Boid->GetNeighbors()) * _WeightAlignment;
		if (!AccumulateForce(_SteeringForce, force)) return _SteeringForce;
	}

	if (On(cohesion))
	{
		force = Cohesion(_Boid->GetNeighbors()) * _WeightCohesion;
		if (!AccumulateForce(_SteeringForce, force)) return _SteeringForce;
	}

	if (On(pursuit))
	{
		force = Pursuit(_TargetAgent1) * _WeightPursuit;
		
		if (!AccumulateForce(_SteeringForce, force)) return _SteeringForce;
	}

	if (On(wander))
	{
		force = Wander(DeltaSeconds) * _WeightWander;
		if (!AccumulateForce(_SteeringForce, force)) return _SteeringForce;
	}

	if (On(offset_pursuit))
	{

		force = OffsetPursuit(_TargetAgent1, _Offset);

		if (!AccumulateForce(_SteeringForce, force)) return _SteeringForce;
	}

	return _SteeringForce;
}

/**
	Calcultes remaining amount of max steering force available to utilise for prioritised steering behaviour

	@FVector &RunningTot
	@FVector ForceToAdd
	@return bool Should force be added
*/
bool SteeringBehaviors::AccumulateForce(FVector &RunningTot, FVector ForceToAdd)
{

	float MagnitudeSoFar = RunningTot.Size();
	float MagnitudeRemaining = _Boid->GetMaxForce() - MagnitudeSoFar;

	// Return false if remaing force depleted
	if (MagnitudeRemaining <= 0.0) return false;

	float MagnitudeToAdd = ForceToAdd.Size();

	// If the magnitude to add does not exceed quota add force
	if (MagnitudeToAdd < MagnitudeRemaining)
	{
		RunningTot += ForceToAdd;
	}
	else
	// Add as much as possible
	{
		ForceToAdd.Normalize();
		RunningTot = RunningTot + (ForceToAdd * MagnitudeRemaining);
	}

	return true;
}

/**
	Calcultes Steering force to direct boid towards target

	@FVector target
	@return FVector SteeringForce
*/
FVector SteeringBehaviors::Seek(FVector target)
{
	FVector boidPos = _Boid->Pos();
	FVector deltaVect = (target - boidPos);
	deltaVect.Normalize();
	FVector DesiredVelocity = deltaVect * _Boid->GetMaxSpeed();
	return (DesiredVelocity - _Boid->Velocity());
}

/**
	Calcultes Steering force to direct boid away target

	@FVector target
	@return FVector SteeringForce
*/
FVector SteeringBehaviors::Flee(FVector target)
{
	FVector boidPos = _Boid->Pos();
	FVector deltaVect = (boidPos - target);
	deltaVect.Normalize();
	FVector DesiredVelocity = deltaVect * _Boid->GetMaxSpeed();
	return (DesiredVelocity - _Boid->Velocity());
}

/**
	Calcultes Steering force to direct boid towards target and arrive with zero velocity

	@FVector target
	@return FVector SteeringForce
*/
FVector SteeringBehaviors::Arrive(FVector target)
{
	FVector ToTarget = target - _Boid->Pos();

	//Distance to the target
	float dist = ToTarget.Size();

	if (dist > 0)
	{
		const float DecelerationTweaker = 0.5f;

		// Calculate speed using deceleration
		float speed = dist / _Boid->GetDecelFractDist() * DecelerationTweaker;

		// Cap velocity
		speed = FMath::Min(speed, _Boid->GetMaxSpeed());

		FVector DesiredVelocity = ToTarget * speed / dist;

		return (DesiredVelocity - _Boid->Velocity());
	}
	// Arrived at target
	return FVector::FVector(0.0f, 0.0f, 0.0f);
}

//------------------------------ Pursuit ---------------------------------
//
//  this behavior creates a force that steers the agent towards the 
//  evader
//------------------------------------------------------------------------

/**
	Calcultes Steering force to direct boid towards a moving entity using their velocity and heading

	@MovingEntity* evader
	@return FVector SteeringForce
*/
FVector SteeringBehaviors::Pursuit(const AMovingEntity* evader)
{

	// The evader is ahead and facing the agent then we can just seek
	FVector ToEvader = evader->Pos() - _Boid->Pos();
	float RelativeHeading = FVector::DotProduct(_Boid->Heading(), evader->Heading());
	if ((FVector::DotProduct(ToEvader, _Boid->Heading()) > 0) && (RelativeHeading < -0.95))
	{
		return Seek(evader->Pos());
	}

	// Otherwise we need to predict where the evader will be
	float LookAheadTime = ToEvader.Size() / (_Boid->GetMaxSpeed() + evader->Speed());
	return Seek(evader->Pos() + evader->Velocity() * LookAheadTime);

}

/**
	Calcultes Steering force to direct boid away from a moving entity using their velocity

	@MovingEntity* pursuer
	@return FVector SteeringForce
*/
FVector SteeringBehaviors::Evade(const AMovingEntity* pursuer)
{

	FVector ToPursuer = pursuer->Pos() - _Boid->Pos();

	// Predict where persuer is going to be
	double LookAheadTime = ToPursuer.Size() / (_Boid->GetMaxSpeed() + pursuer->Speed());

	// Flee from predicted position
	return Flee(pursuer->Pos() + pursuer->Velocity() * LookAheadTime);

}

/**
	Calculate steering force to make boid wander about randomly

	@float DeltaSeconds
	@return FVector SteeringForce
*/
FVector SteeringBehaviors::Wander(float DeltaSeconds)
{

	// Make jitter independent of frame rate
	float JitterThisTimeSlice = _Boid->GetWanderJitter() * DeltaSeconds;

	// Add a small random vector to the target's position
	_WanderTarget += FVector(FMath::FRandRange(-100.0f, 100.0f) * JitterThisTimeSlice, FMath::FRandRange(-100.0f, 100.0f) * JitterThisTimeSlice, FMath::FRandRange(-100.0f, 100.0f) * JitterThisTimeSlice);

	// Reproject on to a unit circle
	_WanderTarget.Normalize();

	// Increase wander magnitude to wander circle
	_WanderTarget *= _Boid->GetWanderRadius();

	FVector targetPos = _WanderTarget + _Boid->Heading() * _Boid->GetWanderDistance();
	FTransform trans = _Boid->GetMesh()->GetComponentTransform();
	FTransform targetTrans = trans.GetRelativeTransform(FTransform::FTransform(targetPos));
	FVector wanderVect = _Boid->Pos() - targetTrans.GetLocation();
	
	return wanderVect;
}

/**
	Calcultes Steering force to direct boid towards specified offset from leader

	@MovingEntity* leader
	@FVector offset
	@return FVector SteeringForce
*/
FVector SteeringBehaviors::OffsetPursuit(AMovingEntity*  leader, const FVector offset)
{
	// Calculate the offset position in world space
	FVector WorldOffsetPos = LocalToWorld(offset, leader);

	FVector ToOffset = WorldOffsetPos - _Boid->Pos();

	// Predict position using own and leader's velocities
	double LookAheadTime = ToOffset.Size() /
		(_Boid->GetMaxSpeed() + leader->Speed());

	// Use arrive
	return Arrive(WorldOffsetPos + leader->Velocity() * LookAheadTime);

}

/**
	Calcultes Steering force to repell boid from neighbors when flocking

	@TArray<AMovingEntity*> &neighbors
	@return FVector SteeringForce
*/
FVector SteeringBehaviors::Separation(const TArray<AMovingEntity*> &neighbors)
{

	FVector SteeringForce = FVector::FVector(0.0f, 0.0f, 0.0f);

	for (int32 a = 0; a < neighbors.Num(); ++a)
	{
		// Exclude self and evaders and make sure it is close enough
		if ((neighbors[a] != _Boid) && (neighbors[a] != _TargetAgent1) && (FVector::DotProduct(neighbors[a]->Heading(), _Boid->Heading()) > 0))
		{
			FVector ToBoid = _Boid->Pos() - neighbors[a]->Pos();
			ToBoid.Normalize();

			// Calculate force inversely proportional to the boid neighbors distance
			SteeringForce = SteeringForce + ToBoid / ToBoid.Size();
		}
	}
	return SteeringForce;
}

/**
	Calcultes Steering force to align boid with neighbors when flocking

	@TArray<AMovingEntity*> &neighbors
	@return FVector SteeringForce
*/
FVector SteeringBehaviors::Alignment(const TArray<AMovingEntity*> &neighbors)
{
	// Average heading of the neighbors
	FVector AverageHeading = FVector::FVector(0.0f, 0.0f, 0.0f);
	int32    NeighborCount = 0;

	//Sum all the neighbors heading vectors  
	for (int32 a = 0; a<neighbors.Num(); ++a)
	{
		// Exclude self and evaders and make sure it is close enough
		if ((neighbors[a] != _Boid) && (neighbors[a] != _TargetAgent1) && (FVector::DotProduct(neighbors[a]->Heading(), _Boid->Heading()) > 0))
		{
			AverageHeading += neighbors[a]->Heading();

			++NeighborCount;
		}
	}

	// Avarage heading by number of neighbors
	if (NeighborCount > 0)
	{
		AverageHeading /= NeighborCount;

		AverageHeading -= _Boid->Heading();
	}
	return AverageHeading;
}

/**
	Calcultes Steering force to move boid towards center of mass of the close neighbors

	@TArray<AMovingEntity*> &neighbors
	@return FVector SteeringForce
*/
FVector SteeringBehaviors::Cohesion(const TArray<AMovingEntity*> &neighbors)
{
	//Find the center of mass of all the boids
	FVector CenterOfMass = FVector::FVector(0.0f, 0.0f, 0.0f);
	FVector SteeringForce = FVector::FVector(0.0f, 0.0f, 0.0f);

	int32 NeighborCount = 0;

	//Sum all the neighbors position vectors
	for (int32 a = 0; a<neighbors.Num(); ++a)
	{
		// Exclude self and evaders and make sure it is close enough
		if ((neighbors[a] != _Boid) && (neighbors[a] != _TargetAgent1) && (FVector::DotProduct(neighbors[a]->Heading(), _Boid->Heading()) > 0))
		{
			CenterOfMass += neighbors[a]->Pos();
			++NeighborCount;
		}
	}

	if (NeighborCount > 0)
	{
		// Center of mass is the average of positions
		CenterOfMass /= NeighborCount;
		SteeringForce = Seek(CenterOfMass);
	}

	SteeringForce.Normalize();
	return SteeringForce;
}