/**
  @Author: Hendranus Vermeulen 
  @Date: 2020-02-25 08:27:56 
  @Last Modified by:   Hendranus Vermeulen 
  @Last Modified time: 2020-02-25 08:27:56 
 
 	SteeringBehaviors encapsulates different steering behaviours and calcultes the combined result of the forces
 
 */

#pragma once
#include "MovingEntity.h"

class STEERING_API SteeringBehaviors
{
public:
	SteeringBehaviors(AMovingEntity* boid);
	~SteeringBehaviors();

	enum summing_method{ weighted_average, prioritized, dithered };

	//Calculate sums of the steering forces
	FVector Calculate(float DeltaSeconds);
	FVector CalculatePrioritized(float DeltaSeconds);
	FVector CalculateWeightedSum(float DeltaSeconds);

	void      SetTarget(FVector t){ _Target = t; }
	void      SetTargetAgent1(AMovingEntity* Agent){ _TargetAgent1 = Agent; }
	void      SetTargetAgent2(AMovingEntity* Agent){ _TargetAgent2 = Agent; }
	void      SetOffset(const FVector offset){ _Offset = offset; }
	FVector  GetOffset()const{ return _Offset; }
	
	FVector Force()const{ return _SteeringForce; }

	//Switch On Steering behaviors
	void FleeOn(){ _Flags |= flee; }
	void SeekOn(){ _Flags |= seek; }
	void ArriveOn(){ _Flags |= arrive; }
	void WanderOn(){ _Flags |= wander; }
	void PursuitOn(AMovingEntity* v){ _Flags |= pursuit; _TargetAgent1 = v; }
	void EvadeOn(AMovingEntity* v){ _Flags |= evade; _TargetAgent1 = v; }
	void CohesionOn(){ _Flags |= cohesion; }
	void SeparationOn(){ _Flags |= separation; }
	void AlignmentOn(){ _Flags |= allignment; }
	void ObstacleAvoidanceOn(){ _Flags |= obstacle_avoidance; }
	void HideOn(AMovingEntity* v){ _Flags |= hide; _TargetAgent1 = v; }
	void OffsetPursuitOn(AMovingEntity* v1, const FVector offset){ _Flags |= offset_pursuit; _Offset = offset; _TargetAgent1 = v1; }
	void FlockingOn(){ CohesionOn(); AlignmentOn(); SeparationOn(); WanderOn(); }

	//Switch Off Steering behaviors
	void FleeOff()  { if (On(flee))   _Flags ^= flee; }
	void SeekOff()  { if (On(seek))   _Flags ^= seek; }
	void ArriveOff(){ if (On(arrive)) _Flags ^= arrive; }
	void WanderOff(){ if (On(wander)) _Flags ^= wander; }
	void PursuitOff(){ if (On(pursuit)) _Flags ^= pursuit; }
	void EvadeOff(){ if (On(evade)) _Flags ^= evade; }
	void CohesionOff(){ if (On(cohesion)) _Flags ^= cohesion; }
	void SeparationOff(){ if (On(separation)) _Flags ^= separation; }
	void AlignmentOff(){ if (On(allignment)) _Flags ^= allignment; }
	void ObstacleAvoidanceOff(){ if (On(obstacle_avoidance)) _Flags ^= obstacle_avoidance; }
	void HideOff(){ if (On(hide)) _Flags ^= hide; }
	void OffsetPursuitOff(){ if (On(offset_pursuit)) _Flags ^= offset_pursuit; }
	void FlockingOff(){ CohesionOff(); AlignmentOff(); SeparationOff(); WanderOff(); }

	//Chaeck if Steering behaviors are active
	bool isFleeOn(){ return On(flee); }
	bool isSeekOn(){ return On(seek); }
	bool isArriveOn(){ return On(arrive); }
	bool isWanderOn(){ return On(wander); }
	bool isPursuitOn(){ return On(pursuit); }
	bool isEvadeOn(){ return On(evade); }
	bool isCohesionOn(){ return On(cohesion); }
	bool isSeparationOn(){ return On(separation); }
	bool isAlignmentOn(){ return On(allignment); }
	bool isObstacleAvoidanceOn(){ return On(obstacle_avoidance); }
	bool isHideOn(){ return On(hide); }
	bool isOffsetPursuitOn(){ return On(offset_pursuit); }

	// Get Weights
	float SeparationWeight(){ return _WeightSeparation; }
	float AlignmentWeight(){ return _WeightAlignment; }
	float CohesionWeight(){ return _WeightCohesion; }

	// Set Weights
	void setWeightSeparation(float v){ _WeightSeparation = v; }
	void setWeightAlignment(float v){ _WeightAlignment = v; }
	void setWeightCohesion(float v){ _WeightCohesion = v; }
	void setWeightWander(float v){ _WeightWander = v; }
	void setWeightObstacleAvoidance(float v){ _WeightObstacleAvoidance = v; }
	void setWeightSeek(float v){ _WeightSeek = v; }
	void setWeightFlee(float v){ _WeightFlee = v; }
	void setWeightArrive(float v){ _WeightArrive = v; }
	void setWeightPursuit(float v){ _WeightPursuit = v; }
	void setWeightOffsetPursuit(float v){ _WeightOffsetPursuit = v; }
	void setWeightHide(float v){ _WeightHide = v; }
	void setWeightEvade(float v){ _WeightEvade = v; }

private:

	enum behavior_type
	{
		none = 0x00000,
		seek = 0x00002,
		flee = 0x00004,
		arrive = 0x00008,
		wander = 0x00010,
		cohesion = 0x00020,
		separation = 0x00040,
		allignment = 0x00080,
		obstacle_avoidance = 0x00100,
		wall_avoidance = 0x00200,
		follow_path = 0x00400,
		pursuit = 0x00800,
		evade = 0x01000,
		interpose = 0x02000,
		hide = 0x04000,
		flock = 0x08000,
		offset_pursuit = 0x10000,
	};

	//Arrive uses these to determine how quickly a boid should decelerate
	enum Deceleration{ slow = 3, normal = 2, fast = 1 };

	//a pointer to the owner of this instance
	AMovingEntity*     _Boid;

	//Steering force combining effect of all behaviors
	FVector    _SteeringForce;

	//Track targets
	AMovingEntity*     _TargetAgent1;
	AMovingEntity*     _TargetAgent2;

	//Current target
	FVector    _Target;

	//Current position on the wander target
	FVector     _WanderTarget;

	//Multipliers to adjusted to strength of the behaviors
	float        _WeightSeparation;
	float        _WeightCohesion;
	float        _WeightAlignment;
	float        _WeightWander;
	float        _WeightObstacleAvoidance;
	float        _WeightSeek;
	float        _WeightFlee;
	float        _WeightArrive;
	float        _WeightPursuit;
	float        _WeightOffsetPursuit;
	float        _WeightHide;
	float        _WeightEvade;

	//Offset used for formations or offset pursuit
	FVector		  _Offset;

	//Binary flags true if behavior should be active
	int			  _Flags;
	
	FVector TruncateVector(FVector &actualVector, float min);

	bool      AccumulateForce(FVector &RunningTot, FVector ForceToAdd);

	//Tests specific bit of _Flags is set
	bool      On(behavior_type bt){ return (_Flags & bt) == bt; }

	FVector Seek(FVector target);

	FVector Flee(FVector target);

	FVector Arrive(FVector TargetPos);

	FVector Pursuit(const AMovingEntity* evader);

	FVector Evade(const AMovingEntity* pursuer);

	FVector LocalToWorld(FVector localPoint, AMovingEntity* actor);

	FVector WorldToLocal(FVector worldPoint, AMovingEntity* actor);

	FVector Wander(float DeltaSeconds);

	//Maintains position and direction as offset from the target
	FVector OffsetPursuit(AMovingEntity* agent, const FVector offset);

	// -- Flocking Behaviors -- //

	FVector Cohesion(const TArray<AMovingEntity*> &agents);

	FVector Separation(const TArray<AMovingEntity*> &agents);

	FVector Alignment(const TArray<AMovingEntity*> &agents);

};
