/**
  @Author: Hendranus Vermeulen 
  @Date: 2020-02-25 08:27:09 
  @Last Modified by:   Hendranus Vermeulen 
  @Last Modified time: 2020-02-25 08:27:09
 
 	Base class from which all steered agents derive, it exposes functions and properties for Blueprint configuration
 
 */

#include "Steering.h"
#include "MovingEntity.h"
#include "DrawDebugHelpers.h"
#include "Math/UnrealMathUtility.h"
// Sets default values
AMovingEntity::AMovingEntity()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	_ID = NextValidID();
}

/**
	Set the boid's heading and use this to set side vector

	@FVector new_heading
*/
void AMovingEntity::SetHeading(FVector new_heading)
{
	if ((new_heading.SizeSquared() - 1.0) < 0.00001)
	{

		_Heading = new_heading;

		//the side vector must always be perpendicular to the heading
		_Side = _Heading.RightVector;
	}
}

/**
	Check to see if heading should be updated and update heading

	@FVector target
	@float DeltaSeconds
	@return bool 
*/
bool AMovingEntity::RotateHeadingToFacePosition(FVector target, float DeltaSeconds)
{

	FVector toTarget = target - _Pos;
	toTarget.Normalize();
	//FString debugString;
	
	FRotator toTargetRotation = toTarget.Rotation();
	FRotator currentRotation = RootComponent->GetComponentRotation();

	//debugString = currentRotation.ToString();
	//UE_LOG(LogClass, Log, TEXT(" currentRotation to  : %s"), *debugString);

	FRotator deltaRotator;

	deltaRotator = toTargetRotation - currentRotation;
	//deltaRotator.Clamp();

	if (currentRotation.Equals(toTargetRotation, 1.0f))
	{
		return true;

	}else{
		
		//DrawDebugDirectionalArrow(GetWorld(), Pos(), (Pos() + toTarget*150.0f), 3.0f, FColor::Blue, false, 0.5f, 0, 2.0f);
		FRotator newRotator;

		newRotator = FMath::RInterpTo(currentRotation, toTargetRotation, DeltaSeconds, MaxTurnRate);

		RootComponent->SetRelativeRotation(newRotator);

		_Heading = GetActorForwardVector();

		_Side = _Heading.RightVector;

		return false;
	}
}