/**
  @Author: Hendranus Vermeulen 
  @Date: 2020-02-25 08:26:28 
  @Last Modified by:   Hendranus Vermeulen 
  @Last Modified time: 2020-02-25 08:26:28 
 
 	Boid extends MovingEntity adding detection spherese and functions, exposing additional functions and properties for Blueprint configuration
 
 */

#include "Steering.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/DefaultPawn.h"
#include "DrawDebugHelpers.h"
#include "Boid.h"

ABoid::ABoid()
{
	// Structure to hold one-time initialization
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UStaticMesh> boidMesh;
		FConstructorStatics()
			: boidMesh(TEXT("/Game/Flying/Meshes/UFO.UFO"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;

	// Create static mesh component
	boidMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PlaneMesh0"));
	boidMesh->SetStaticMesh(ConstructorStatics.boidMesh.Get());
	RootComponent = boidMesh;

	SteerBehavior = new SteeringBehaviors(this);

	neighbourSphere = CreateDefaultSubobject<USphereComponent>(TEXT("NeighborDetectSphere"));
	neighbourSphere->AttachTo(RootComponent);
	neighbourSphere->SetSphereRadius(500.0f);

	obstableDetectCaps = CreateDefaultSubobject<UCapsuleComponent>(TEXT("ObstackeCapsule"));
	obstableDetectCaps->AttachTo(RootComponent);

	obstableDetectCaps->SetCapsuleSize(100.0f, 100.0f);
	FTransform trans = FTransform::FTransform();
	trans.SetRotation(FQuat::FQuat(FRotator::FRotator(90.0f, 0.0f, 0.0f)));
	trans.SetLocation(FVector::FVector(50.0f, 0.0f, 0.0f));
	obstableDetectCaps->SetRelativeTransform(trans);
}

void ABoid::InitialiseNeigborsTArray()
{
	neighbors.Empty();
	TArray<AActor*> overlappingActors;
	neighbourSphere->GetOverlappingActors(overlappingActors);
	for (int32 i = 0; i < overlappingActors.Num(); i++)
	{
		AMovingEntity* boid = Cast<AMovingEntity>(overlappingActors[i]);
		if (boid && !boid->IsPendingKill() && boid != this)
		{
			neighbors.AddUnique(boid);
		}
	}
	updateNeighborTArray();
}

void ABoid::updateNeighborTArray()
{
	for (int32 j = 0; j < neighbors.Num(); j++)
	{
		FString debugString = neighbors[j]->GetDebugName(neighbors[j]);
		UE_LOG(LogClass, Log, TEXT(" updateNeighborTArray IS : %s"), *debugString);
	}
}

void ABoid::NeighborOverlapStart(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	//FString debugString = OtherActor->GetDebugName(OtherActor);
	//UE_LOG(LogClass, Log, TEXT(" NeighborOverlapStart : %s"), *debugString);
	AMovingEntity* boid = Cast<AMovingEntity>(OtherActor);
	if (boid && !boid->IsPendingKill())
	{
		//FString debugString = OtherActor->GetDebugName(OtherActor);
		//UE_LOG(LogClass, Log, TEXT(" NeighborOverlapStart add : %s"), *debugString);
		AMovingEntity* boid = Cast<AMovingEntity>(OtherActor);
		neighbors.AddUnique(boid);
	}
}

void ABoid::NeighborOverlapEnd(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	//FString debugString = OtherActor->GetDebugName(OtherActor);
	//UE_LOG(LogClass, Log, TEXT(" NeighborOverlapEnd : %s"), *debugString);
	AMovingEntity* boid = Cast<AMovingEntity>(OtherActor);
	if (boid && !boid->IsPendingKill())
	{
		//FString debugString = OtherActor->GetDebugName(OtherActor);
		//UE_LOG(LogClass, Log, TEXT(" NeighborOverlapEnd remove : %s"), *debugString);
		AMovingEntity* boid = Cast<AMovingEntity>(OtherActor);
		neighbors.Remove(boid);
	}
}

// Called when the game starts or when spawned
void ABoid::BeginPlay()
{
	Initialise();
	SetPos(GetActorLocation());
	Super::BeginPlay();
	InitialiseNeigborsTArray();
}

void ABoid::Initialise(){
	SteerBehavior->FleeOff();
	SteerBehavior->SeekOff();
	SteerBehavior->ArriveOff();
	SteerBehavior->WanderOff();
	SteerBehavior->PursuitOff();
	SteerBehavior->EvadeOff();
	SteerBehavior->CohesionOff();
	SteerBehavior->SeparationOff();
	SteerBehavior->AlignmentOff();
	SteerBehavior->ObstacleAvoidanceOff();
	SteerBehavior->HideOff();
	SteerBehavior->OffsetPursuitOff();
	SteerBehavior->FlockingOff();
	SteerBehavior->setWeightSeparation(WeightSeparation);
	SteerBehavior->setWeightAlignment(WeightAlignment);
	SteerBehavior->setWeightCohesion(WeightCohesion);
	SteerBehavior->setWeightWander(WeightWander);
	SteerBehavior->setWeightObstacleAvoidance(WeightObstacleAvoidance);
	SteerBehavior->setWeightSeek(WeightSeek);
	SteerBehavior->setWeightFlee(WeightFlee);
	SteerBehavior->setWeightArrive(WeightArrive);
	SteerBehavior->setWeightPursuit(WeightPursuit);
	SteerBehavior->setWeightOffsetPursuit(WeightOffsetPursuit);
	SteerBehavior->setWeightHide(WeightHide);
	SteerBehavior->setWeightEvade(WeightEvade);
}

/**
	Updates Boid's position

	@float DeltaSeconds
*/
void ABoid::UpdatePos(float DeltaSeconds)
{
	FVector SteeringForce;

	// Calculate the combined force from each steering behavior
	SteeringForce = SteerBehavior->Calculate(DeltaSeconds);

	//Acceleration = Force/Mass
	FVector acceleration = SteeringForce * ForceMultiplier / Mass;

	//update velocity
	_Velocity += acceleration * DeltaSeconds;

	//make sure vehicle does not exceed maximum velocity
	//FString debugString;
	//debugString = FString::SanitizeFloat(SteeringForce.Size());
	//UE_LOG(LogClass, Log, TEXT("My SteeringForce : %s"), *debugString);
	float speed = _Velocity.Size();
	if (speed > MaxSpeed)
	{
		_Velocity.Normalize();
		_Velocity *= MaxSpeed;
		speed = _Velocity.Size();
	}

	if (FMath::RoundToInt(oldSpeed) != FMath::RoundToInt(speed))
	{
		obstableDetectCaps->SetCapsuleSize(100.0f, speed);
		FTransform trans = FTransform::FTransform();
		trans.SetRotation(FQuat::FQuat(FRotator::FRotator(90.0f, 0.0f, 0.0f)));
		trans.SetLocation(FVector::FVector(speed, 0.0f, 0.0f));
		obstableDetectCaps->SetRelativeTransform(trans);
		oldSpeed = FMath::RoundToInt(speed);
		FString debugString;
		debugString = FString::SanitizeFloat(oldSpeed);
		//UE_LOG(LogClass, Log, TEXT("Speed change to  : %s"), *debugString);
	}
	
	// Update position
	_Pos += _Velocity * DeltaSeconds;
	RootComponent->SetWorldLocation(_Pos);

	// Update heading
	if (_Velocity.SizeSquared() > 0.00000001)
	{
		_Velocity.Normalize();
		_Heading = _Velocity;

		//FString debugString;
		//debugString = m_vHeading.ToString();
		//UE_LOG(LogClass, Log, TEXT("My m_vHeading : %s"), *debugString);

		_Side = _Heading.RightVector;
	}

}



