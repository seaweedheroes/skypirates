/**
  @Author: Hendranus Vermeulen 
  @Date: 2020-02-25 08:26:53 
  @Last Modified by:   Hendranus Vermeulen 
  @Last Modified time: 2020-02-25 08:26:53
 
 	Boid extends MovingEntity adding detection spherese and functions, exposing additional functions and properties for Blueprint configuration
 
 */

#pragma once

#include "MovingEntity.h"
#include "SteeringBehaviors.h"
#include "Boid.generated.h"

UCLASS()
class STEERING_API ABoid : public AMovingEntity
{
	GENERATED_BODY()

	// Sphere component that will check neighour boids
	UPROPERTY(VisibleAnywhere, Category = Mesh, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USphereComponent* neighbourSphere;

	// Sphere component that will check neighour boids
	UPROPERTY(VisibleAnywhere, Category = Mesh, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UCapsuleComponent* obstableDetectCaps;
	
public:
	ABoid();

	// Updates Boid position
	UFUNCTION(BlueprintCallable, Category = "MovingEntity")
	void UpdatePos(float DeltaSeconds);

	//-------------------------------------------accessor methods
	SteeringBehaviors*const  Steering()const{ return SteerBehavior; }

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void Initialise();
	UFUNCTION(BlueprintCallable, Category = "Steering")
		void ObstacleOverlapStart(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION(BlueprintCallable, Category = "Steering")
		void ObstacleOverlapEnd(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintCallable, Category = "Steering")
	void NeighborOverlapStart(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void NeighborOverlapEnd(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintCallable, Category = "Steering")
	USphereComponent* GetNeighbourSphere()  { return neighbourSphere; }

	UFUNCTION(BlueprintCallable, Category = "Steering")
	UCapsuleComponent* GetObstacleCapsule()  { return obstableDetectCaps; }

	UFUNCTION(BlueprintCallable, Category = "Steering")
	void      SetTarget(FVector t){ SteerBehavior->SetTarget(t); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void      SetTargetAgent1(AMovingEntity* Agent){ SteerBehavior->SetTargetAgent1(Agent); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void      SetTargetAgent2(AMovingEntity* Agent){ SteerBehavior->SetTargetAgent2(Agent); }

	UFUNCTION(BlueprintCallable, Category = "Steering")
	void FleeOn(){ SteerBehavior->FleeOn(); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void SeekOn(){ SteerBehavior->SeekOn(); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void ArriveOn(){ SteerBehavior->ArriveOn(); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void WanderOn(){ SteerBehavior->WanderOn(); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void PursuitOn(AMovingEntity* movingEntity){ SteerBehavior->PursuitOn(movingEntity); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void EvadeOn(AMovingEntity* movingEntity){ SteerBehavior->EvadeOn(movingEntity); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void CohesionOn(){ SteerBehavior->CohesionOn(); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void SeparationOn(){ SteerBehavior->SeparationOn(); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void AlignmentOn(){ SteerBehavior->AlignmentOn(); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void ObstacleAvoidanceOn(){ SteerBehavior->ObstacleAvoidanceOn(); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void HideOn(AMovingEntity* v){ SteerBehavior->HideOn(v); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void OffsetPursuitOn(AMovingEntity* movingEntity, const FVector offset){ SteerBehavior->OffsetPursuitOn(movingEntity, offset); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void FlockingOn(){ SteerBehavior->FlockingOn(); }

	UFUNCTION(BlueprintCallable, Category = "Steering")
	void FleeOff(){ SteerBehavior->FleeOff(); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void SeekOff(){ SteerBehavior->SeekOff(); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void ArriveOff(){ SteerBehavior->ArriveOff(); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void WanderOff(){ SteerBehavior->WanderOff(); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void PursuitOff(){ SteerBehavior->PursuitOff(); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void EvadeOff(){ SteerBehavior->EvadeOff(); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void CohesionOff(){ SteerBehavior->CohesionOff(); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void SeparationOff(){ SteerBehavior->SeparationOff(); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void AlignmentOff(){ SteerBehavior->AlignmentOff(); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void ObstacleAvoidanceOff(){ SteerBehavior->ObstacleAvoidanceOff(); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void HideOff(){ SteerBehavior->HideOff(); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void OffsetPursuitOff(){ SteerBehavior->OffsetPursuitOff(); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
	void FlockingOff(){ SteerBehavior->FlockingOff(); }

	UFUNCTION(BlueprintCallable, Category = "Steering")
	void setBoxLength(float BoxLength){ SteerBehavior->setBoxLength(BoxLength); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
		void setWeightSeparation(float SeparationWeight){ SteerBehavior->setWeightSeparation(SeparationWeight); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
		void setWeightAlignment(float AlignmentWeight){ SteerBehavior->setWeightAlignment(AlignmentWeight); }
	UFUNCTION(BlueprintCallable, Category = "Steering")
		void WeightsetCsetWeightCohesionohesion(float CohesionWeight){ SteerBehavior->setWeightCohesion(CohesionWeight); }

private:

	// Steering behavior class
	SteeringBehaviors*     SteerBehavior;
	float oldSpeed;
	float ForceMultiplier = 100000.0f;

	void updateNeighborTArray();
	void InitialiseNeigborsTArray();
};
