# Sky Pirates VR Game Prototype #

VR game prototype using UE4 and Leap Motion for hand tracking and player input for rocket-propelled flight. Flight behaviours for enemies and allies were developed in C++ exposing functionality to Blueprints (based on Mat Buckland's 'Programming Game AI by Example' examples for Craig Reynolds' Flocking behaviours).

[![Watch the video](https://img.youtube.com/vi/7w2-YAg21h4/maxresdefault.jpg)](https://youtu.be/7w2-YAg21h4)